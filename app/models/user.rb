class User < ActiveRecord::Base
  has_many :projects

  before_save { self.email	=	email.downcase }
  has_secure_password

  validates :name, presence: true
  validates :password, length: { minimum: 4 }
end
