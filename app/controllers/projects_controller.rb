require 'aws-sdk'

class ProjectsController < ApplicationController
  def start
    @projects = current_projects
    if logged_in?
    else
      redirect_to '/login'
    end
  end

  def new
    @projects = current_projects
    @project = Project.new
  end

  def create
    @project = Project.new(project_params)
    @project.state = 'enabled'

    @project.user = current_user

    if @project.save
      redirect_to "/projects/#{@project.id}"
    else

    end
  end

  def edit
    puts "request.referrer: #{request.referrer}"
    @projects = current_projects

    if params[:id] != 'base'
      @project = Project.find(params[:id])
      @attachment = Attachment.new

      result = Attachment.where('entity_id = ? AND entity_type = ?', @project.id, 'project')
      if result.size == 0
        @attachment.url = ''
      else
        @attachment = result[0]
      end
    end

  end


  ### UPDATE PROJECT
  ##################
  def update
    image = params[:project][:avatar]
    @project = Project.find(params[:project][:id])

    @project.title = params[:project][:title]
    @project.blurb = params[:project][:blurb]
    @project.category = params[:project][:category]
    @project.duration = params[:project][:duration]
    @project.goal = params[:project][:goal]

    if @project.save
      if image.present?
        s3 = Aws::S3::Resource.new(
            credentials: Aws::Credentials.new(ENV.fetch('AWS_ACCESS_KEY_ID'), ENV.fetch('AWS_SECRET_ACCESS_KEY')),
            region: ENV.fetch('AWS_REGION')
        )
        key = "GH/User#{current_user.id}/Project#{@project.id}/#{image.original_filename}"
        obj = s3.bucket(ENV.fetch('S3_BUCKET_NAME')).object(key)
        obj.upload_file(image.tempfile, acl:'public-read')

        @attachment = Attachment.new

        @attachment.entity_id = @project.id
        @attachment.entity_type = 'project'
        @attachment.url = obj.public_url
        @attachment.state = 'enabled'

        if @attachment.save
          @project.url = @attachment.url
          if @project.save
            redirect_to "/projects/#{@project.id}"
          end
        end
      else
        redirect_to "/projects/#{@project.id}"
      end
    end
  end

  def show
    @projects = current_projects
    @project = Project.find(params[:id])
    @user = User.find(@project.user_id)
    @attachment = Attachment.new

    result = Attachment.where('entity_id = ? AND entity_type = ?', @project.id, 'project')
    if result.size == 0
      @attachment.url = 'https://ksr-ugc.imgix.net/missing_project_photo.png?w=218&h=122&fit=fill&bg=000000&v=&auto=format&q=92&s=0cbcc777acf757d672746609f507c33b'
    else
      @attachment = result[0]
    end
  end



  private
    def project_params
      params.require(:project).permit(:id, :title, :blurb, :category, :location, :duration, :goal)
    end
end
