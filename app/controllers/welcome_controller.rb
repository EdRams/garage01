class WelcomeController < ApplicationController
  def index
    if logged_in?
      @projects = current_projects
      puts "SIZE: #{@projects.size}"
      @current_user = current_user
    end

    @last_projects = Project.where('state = ?', 'enabled').order('created_at DESC')
  end
end
