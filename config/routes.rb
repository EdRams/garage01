Rails.application.routes.draw do
  root 'welcome#index'

  get 'sessions/new'
  get 'signup' => 'users#new'

  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  get '/users/logout' => 'sessions#destroy'

  resources :users


  get '/projects' => 'projects#start'
  get '/projects/new' => 'projects#new'
  post '/projects' => 'projects#create'
  get '/projects/:id' => 'projects#edit'
  patch '/projects/:id' => 'projects#update'
  get '/projects/public/:id' => 'projects#show'
end
