class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.integer :entity_id
      t.string :entity_type
      t.text :url
      t.string :state

      t.timestamps null: false
    end
  end
end
