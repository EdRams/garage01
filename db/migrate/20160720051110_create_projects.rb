class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.integer :user_id

      t.string :title
      t.text :blurb
      t.text :location
      t.string :duration
      t.string :goal
      t.string :state

      t.timestamps null: false
    end
  end
end
